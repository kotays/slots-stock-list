import cookieParser from 'cookie-parser';
import session from 'express-session';
import bodyParser from 'body-parser';
import path from 'path';

const envConfig = require('dotenv').config({ path: path.resolve(__dirname, '../.env') });
const APP_CONFIG = envConfig.parsed || {};

const express = require('express');
const app = express();

const http = require('http').Server(app);
const io = require('socket.io')(http);

const routes = require('./routes');
const initSocket = require('./socket');

const staticPath = path.resolve(__dirname, '../build');
const imgPath = path.resolve(__dirname, '../bgimages');
const indexPath = staticPath + '/index.html';

app
  .use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization');
    next();
  })
  .use(cookieParser())
  .use(session({
      secret: 'asntewiu432rwqDAseqwe',
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 1000 * 60 * 60 * 24 }
    }
  ))
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(express.static(staticPath))
  .use(express.static(imgPath))
  .use('/api', routes)
  .use((req, res) => { res.sendFile(indexPath); });

initSocket(io, app);

const SERVER_PORT = APP_CONFIG.REACT_APP_SERVER_PORT || 3001;
http.listen(SERVER_PORT, () => {
  console.log(`Server Started on *:${SERVER_PORT}`);
});
