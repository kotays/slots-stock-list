export const NO_PERMISSION = 'Нет прав';

export const ERROR_USER = 'Неверный логин/пароль';
export const ERROR_USER_NOT_FOUND = 'Пользователь не найден';
export const ERROR_CREATE_USER = 'Все поля обязательны для заполнения';

export const ERROR_ZAL_NOT_FOUND = 'Зал не найден';
export const ERROR_ZAL_FILL = 'У зала должно быть имя!';


