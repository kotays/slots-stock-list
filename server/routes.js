import { Router } from 'express';
import fs from 'fs';
import { forEach, get, omit } from 'lodash';
import shortid from 'shortid';
import multer from 'multer';

import { Images, Users, Zals, Stats } from './db';

import {
  ERROR_USER,
  ERROR_CREATE_USER,
  ERROR_USER_NOT_FOUND,
  ERROR_ZAL_FILL,
  ERROR_ZAL_NOT_FOUND,
  NO_PERMISSION,
} from './errors';

const router = Router();

const getCurrentUser = req => {
  const userId = req.session['user_id'];
  if (!userId) return false;

  return Users.find({ id: userId }).value();
};

router.post('/profile', (req, res) => {
  const user = getCurrentUser(req);
  if (!user) return res.send(null);

  res.send({ profile: user });
});

router.post('/logout', (req, res) => {
  req.session['user_id'] = null;
  res.send();
});

router.post('/login', (req, res) => {
  const { login, pass } = req.body;
  if (!login || !pass) return res.send({ error: ERROR_USER });

  const user = Users.find({ login, pass }).value();
  if (!user) return res.send({ error: ERROR_USER });

  req.session['user_id'] = user.id;

  res.send({ success: user });
});

router.get('/get_all_users', (req, res) => {
  const user = getCurrentUser(req);
  if (!user || !user.isAdmin) return res.send({ error: NO_PERMISSION });

  const usersList = Users.value();
  res.send({ users: usersList });
});

router.post('/update_user', (req, res) => {
  const { id, name, login, pass } = req.body;

  const user = getCurrentUser(req);
  if (!user || !user.isAdmin) return res.send({ error: NO_PERMISSION });

  if (!name || !login || !pass) return res.send({ error: ERROR_CREATE_USER });

  if (id === 'new') {
    Users
      .push({ id: shortid.generate(), name, login, pass })
      .write();

    return res.send({ success: 1 });
  }

  const editUser = Users.find({ id });
  if (!editUser) return res.send({ error: ERROR_USER_NOT_FOUND });

  editUser
    .assign({ name, login, pass })
    .write();

  res.send({ success: 1 });
});

router.post('/delete_user', (req, res) => {
  const { id } = req.body;

  const user = getCurrentUser(req);
  if (!user || !user.isAdmin || user.id === id) return res.send({ error: NO_PERMISSION });

  const foundUser = Users.find({ id });
  if (!foundUser) return res.send({ error: ERROR_USER_NOT_FOUND });

  Users.remove({ id }).write();

  res.send({ success: 1 });
});

router.get('/get_all_zals', (req, res) => {
  const zals = Zals.value();
  const users = Users.value();
  const images = Images.value();
  res.send({ zals, users, images });
});

router.post('/get_zal', (req, res) => {
  const { zal_id } = req.body;

  const zal = Zals.find({ id: zal_id }).value();
  if (!zal) res.send({ error: ERROR_ZAL_NOT_FOUND });

  res.send({ zal: omit(zal, 'allowUsers') });
});

router.post('/get_user_zals', (req, res) => {
  const { user_id } = req.body;

  const zals = Zals
    .filter(zal => {
      if (!user_id) return zal;
      if (!zal.allowUsers) return false;

      return zal.allowUsers.includes(user_id)
    })
    .value()
    .map(zal => {
      if (zal.imageId) {
        zal.imageSrc = get(Images.find({ id: zal.imageId }).value(), 'src');
      }
      return zal;
    });

  res.send({ zals });
});

router.post('/save_zal', (req, res) => {
  const { id, name, allowUsers, imageId } = req.body;

  const user = getCurrentUser(req);
  if (!user || !user.isAdmin) return res.send({ error: NO_PERMISSION });

  if (!name) return res.send({ error: ERROR_ZAL_FILL });

  if (id === 'new') {
    Zals
      .push({ id: shortid.generate(), name, allowUsers, imageId })
      .write();

    return res.send({ success: 1 });
  }

  const zal = Zals.find({ id });
  if (!zal) return res.send({ error: ERROR_ZAL_NOT_FOUND });
  zal.assign({ name, allowUsers, imageId }).write();

  res.send({ success: 1 });
});

router.post('/delete_zal', (req, res) => {
  const { id } = req.body;

  const user = getCurrentUser(req);
  if (!user || !user.isAdmin) return res.send({ error: NO_PERMISSION });

  const foundUser = Zals.find({ id });
  if (!foundUser) return res.send({ error: ERROR_USER_NOT_FOUND });

  Zals.remove({ id }).write();

  res.send({ success: 1 });
});

router.get('/get_all_images', (req, res) => {
  res.send({ images: Images.value() });
});

const BASE_BGS_PATH = '/bgimages';
const BUILD_PATH = process.env.NODE_ENV === 'production' ? 'build' : 'public';
const GLOBAL_BGS_PATH = `../${BUILD_PATH}${BASE_BGS_PATH}`;

const upload = multer({ dest: GLOBAL_BGS_PATH }).array('files');
router.post('/upload_img', (req, res) => {
  const user = getCurrentUser(req);
  if (!user || !user.isAdmin) return res.send({ error: NO_PERMISSION });

  upload(req, res, err => {
    if (err) return res.send({ error: err });

    forEach(req.files, file => {
      Images
        .push({
          id: shortid.generate(),
          src: `${BASE_BGS_PATH}/${file.filename}`,
          name: file.originalname,
          path: `${GLOBAL_BGS_PATH}/${file.filename}`,
        })
        .write();
    });

    res.send({ success: 1 });
  });
});

router.post('/delete_image', (req, res) => {
  const { id } = req.body;

  if (!id) return res.send({ error: NO_PERMISSION });

  const user = getCurrentUser(req);
  if (!user || !user.isAdmin) return res.send({ error: NO_PERMISSION });

  const image = Images.find({ id }).value();
  if (!image) return res.send({ error: 'Файла не существует' });

  fs.unlink(image.path, err => {
    if (err) return res.send({ error: err });

    Images.remove({ id }).write();
    res.send({ success: 1 });
  });
});

router.post('/close_zal_and_save', (req, res) => {
  const user = getCurrentUser(req);
  if (!user) return res.send({ error: NO_PERMISSION });

  const zal = Zals.find({ id: req.body.zalId });
  if (!zal.value()) return res.send({ error: ERROR_ZAL_NOT_FOUND });

  const zalValue = zal.value();

  const isUserAllowSave = (zalValue.allowUsers || []).includes(user.id);
  if (!isUserAllowSave) return res.send({ error: NO_PERMISSION });

  let zalStats = Stats.find({ zal_id: zalValue.id });
  if (!zalStats.value()) {
    Stats.push({ zal_id: zalValue.id, zal_name: zalValue.name, days: [] }).write();
    Stats.find({ zal_id: zalValue.id });
  }

  const days = get(zalStats.value(), 'days') || [];
  const newDay = { date: new Date(), players: zalValue.zalUsers };

  zalStats.assign({ days: [...days, newDay] }).write();
  zal.assign({ zalUsers: [] }).write();

  res.send({ success: 1 });
});

router.post('/get_all_zals_stat', (req, res) => {
  const user = getCurrentUser(req);
  if (!user || !user.isAdmin) return res.send({ error: NO_PERMISSION });
  res.send({ data: Stats.valueOf() });
});

module.exports = router;
