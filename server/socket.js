import { JOIN_TO_ZAL, UPDATE_ZAL_USERS } from '../src/constants/socket-actions';
import { Zals } from './db';

module.exports = function initSocket(io, app) {
  io.on('connection', socket => {
    socket.on(JOIN_TO_ZAL, zalId => {
      if (zalId) {
        socket.zalId = zalId;
        socket.join(zalId);
      }
    });

    socket.on(UPDATE_ZAL_USERS, ({ zalId, zalUsers, save }) => {
      if (socket.zalId) {
        socket.broadcast.to(socket.zalId).emit(UPDATE_ZAL_USERS, zalUsers);
      }

      if (save) {
        Zals
          .find({ id: zalId })
          .assign({ zalUsers })
          .write()
      }
    });

  })
};
