const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json');
const db = low(adapter);

db.defaults({ zals: [], users: [], images: [], stats: [], }).write();

export const Zals = db.get('zals');
export const Users = db.get('users');
export const Images = db.get('images');
export const Stats = db.get('stats');

export default db;
