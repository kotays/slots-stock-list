import React, { PureComponent } from 'react';
import DatePicker from 'react-datepicker';
import { Button } from 'reactstrap';

import Loader from '../../components/loader';
import fetcher from '../../helpers/fetcher';

class AdminStats extends PureComponent {
  state = {
    zals: [],
    selectedId: null,
    loading: true,
    startDate: new Date(),
    endDate: new Date(),
  };

  componentDidMount() {
    fetcher
      .post('/get_all_zals_stat')
      .then(res => {
        if (res.error) return alert(res.error);
        this.setState({ zals: res.data, loading: false });
      })
  }

  getZalsList = () => {
    const { zals, selectedId } = this.state;
    const selectZal = selectedId => this.setState({ selectedId });

    return (
      <div className="admin-stats-zals-list">
        {
          zals.map(zal => (
            <Button
              key={zal.id}
              className="zal-btn"
              color={selectedId === zal.id ? 'success' : 'secondary'}
              onClick={() => selectZal(zal.id)}
            >{zal.name}</Button>
          ))
        }
      </div>
    );
  };

  getDateFilters = () => {
    const { selectedId, startDate, endDate } = this.state;
    if (!selectedId) return false;

    return (
      <div className="date-filters">
        <DatePicker
          selectsStart
          selected={startDate}
          startDate={startDate}
          endDate={endDate}
          onChange={this.handleChangeStart}
        />

        <DatePicker
          selectsEnd
          selected={endDate}
          startDate={startDate}
          endDate={endDate}
          onChange={this.handleChangeEnd}
        />
      </div>
    )
  };

  render () {
    const { selectedZal, startDate, endData, loading } = this.state;
    if (loading) return <Loader/>;

    return (
      <div className="admin-zal-stats">
        {this.getZalsList()}
        {this.getDateFilters()}
      </div>
    );
  }
}

export default AdminStats;

