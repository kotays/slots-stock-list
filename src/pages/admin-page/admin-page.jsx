import React, { useState } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { isEqual } from 'lodash';
import classNames from 'classnames';

import './admin-page.scss';
import withProfile from '../../hocs/withProfile';

import AdminUsersPage from './admin-users';
import AdminZalsPage from './admin-zals';
import AdminPhotosPage from './admin-photos';
import AdminStats from './admin-stats';


const AdminPage = ({ profile }) => {
  if (!profile) return null;
  if (!profile.isAdmin) return window.location.href = "/404";

  const [tab, updateTab] = useState('4');

  return (
    <div className="admin-page-wrapper">
      <h1>Admin <Link to="/" style={{ fontSize: 15 }}>Домой</Link></h1>
      <Nav tabs>
        <NavItem>
          <NavLink className={classNames({ active: isEqual(tab, '2') })} onClick={() => updateTab('2')}>
            Пользователи
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink className={classNames({ active: isEqual(tab, '1') })} onClick={() => updateTab('1')}>
            Залы
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink className={classNames({ active: isEqual(tab, '3') })} onClick={() => updateTab('3')}>
            Фоновые картинки
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink className={classNames({ active: isEqual(tab, '4') })} onClick={() => updateTab('4')}>
            Статистика
          </NavLink>
        </NavItem>
      </Nav>
      <div className="admin-page-content">
        { tab === '2' && <AdminUsersPage profile={profile} /> }
        { tab === '1' && <AdminZalsPage profile={profile} /> }
        { tab === '3' && <AdminPhotosPage profile={profile} /> }
        { tab === '4' && <AdminStats profile={profile} /> }
      </div>
    </div>
  );
};

export default withProfile(AdminPage);
