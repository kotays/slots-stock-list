import React, { PureComponent } from 'react';
import { Button } from 'reactstrap';
import AdminUser from '../../components/admin-user';
import Loader from '../../components/loader';
import fetcher from '../../helpers/fetcher';

class AdminUsersPage extends PureComponent {

  state = {
    users: [],
    addNew: false,
    loading: true,
  };

  componentDidMount() {
    this.fetchUsers();
  }

  fetchUsers = () => {
    fetcher
      .get('/get_all_users')
      .then(data => {
        if (data && data.users) {
          const { users } = data || { users: [] };
          this.setState({ users, loading: false, addNew: false })
        } else if (data.error) {
          alert(data.error);
        }
      })
  };

  getActions = () => {
    const { addNew } = this.state;
    return (
      <div className="admin-add-new-user">
        { !addNew && <Button color="success" onClick={() => { this.setState({ addNew: true }) }}>Создать</Button> }
        { addNew && <Button color="danger" onClick={() => { this.setState({ addNew: false }) }}>Отмена</Button> }
      </div>
    )
  };

  onSaveUser = user => {
    fetcher
      .post('/update_user', user)
      .then(data => {
        if (data.success) this.fetchUsers();
        else if (data.error) alert(data.error);
      });
  };

  onDeleteUser = user => {
    if (window.confirm("Вы дейтвительно хотите удалить пользователя")) {
      fetcher
        .post('/delete_user', user)
        .then(data => {
          if (data.success) this.fetchUsers();
          else if (data.error) alert(data.error);
        });
    }
  };

  getUsersList = () => {
    const { users, profile, addNew } = this.state;

    return (
      <div className="admin-users-list">
        {
          addNew && (
            <AdminUser
              key="new"
              onSave={this.onSaveUser}
              onDelete={this.onDeleteUser}
              user={{ id: 'new' }}
              currentUserId={profile && profile.id} />
          )
        }
        {
          users.map(user => (
            <AdminUser
              key={user.id}
              onSave={this.onSaveUser}
              onDelete={this.onDeleteUser}
              user={user}
              currentUserId={profile && profile.id} />
          ))
        }
      </div>
    )
  };

  render() {
    const { loading } = this.state;
    if (loading) return <Loader />;

    return (
      <div className="users-page-wrapper">
        <div className="actions-block">
          { this.getActions() }
        </div>
        { this.getUsersList() }
      </div>
    )
  };
}

export default AdminUsersPage;
