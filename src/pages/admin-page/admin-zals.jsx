import React, { PureComponent } from 'react';
import { Button } from 'reactstrap';
import fetcher from '../../helpers/fetcher';
import AdminZal from '../../components/admin-zal';

class AdminZalsPage extends PureComponent {
  state = {
    zals: [],
    users: [],
    images: [],
    addNew: false,
    loading: true,
  };

  componentDidMount() {
    this.fetchZals();
  }

  fetchZals = () => {
    fetcher
      .get('/get_all_zals')
      .then(data => {
        if (data.zals) {
          this.setState({
            zals: data.zals || [],
            users: data.users || [],
            images: data.images || [],
            loading: false,
            addNew: false,
          });
        } else if (data.error) {
          alert(data.error);
        }
      })
  };

  saveZal = zal => {
    fetcher
      .post('/save_zal', zal)
      .then(data => {
        if (data.success) this.fetchZals();
        else if ( data.error ) alert(data.error);
      })
  };

  deleteZal = zal => {
    fetcher
      .post('/delete_zal', zal)
      .then(data => {
        if (data.success) this.fetchZals();
        else if ( data.error ) alert(data.error);
      })
  };

  renderActions = () => {
    const { addNew } = this.state;
    return (
      <div className="admin-add-new-user">
        { !addNew && <Button color="success" onClick={() => { this.setState({ addNew: true }) }}>Создать</Button> }
        { addNew && <Button color="danger" onClick={() => { this.setState({ addNew: false }) }}>Отмена</Button> }
      </div>
    )
  };

  renderZalsList = () => {
    const { zals, addNew, users, images } = this.state;

    return (
      <div className="admin-zals-list">
        {
          addNew && (
            <AdminZal
              key="new"
              zal={{ id: 'new' }}
              users={users}
              images={images}
              onSave={this.saveZal}
              onDelete={this.deleteZal}
            />
          )
        }
        {
          zals.map(zal => (
            <AdminZal
              key={zal.id}
              zal={zal}
              users={users}
              images={images}
              onSave={this.saveZal}
              onDelete={this.deleteZal}
            />
          ))
        }
      </div>
    )
  };

  render() {
    return (
      <div className="admin-zals-page">
        {this.renderActions()}
        {this.renderZalsList()}
      </div>
    )
  };
}

export default AdminZalsPage;
