import React, { PureComponent } from 'react';
import Loader from '../../components/loader';
import fetcher from '../../helpers/fetcher';
import { Button } from 'reactstrap';
import { forEach } from 'lodash';

class AdminPhotos extends PureComponent {
  state = {
    images: [],
    uploadImages: [],
    loading: true,
    uploadLoading: false,
  };

  componentDidMount() {
    this.fetchImages();
  }

  fetchImages = () => {
    fetcher
      .get('/get_all_images')
      .then(res => {
        if (res.images) {
          this.setState({
            images: res.images,
            loading: false,
            uploadImages: [],
            uploadLoading: false
          });
        }
        else this.setState({ loading: false });
      })
  };

  onFileInputChange = e => {
    const { files } = e.target;

    if (!files || !files.length) {
      if (this.state.uploadImages.length > 0) this.setState({ uploadImages: [] });
      return;
    }

    let downLoaded = 0;
    const uploadImages = [];

    const checkFiles = () => {
      if (downLoaded === files.length) {
        this.setState({ uploadImages });
      }
    };

    forEach(files, file => {
      const reader = new FileReader();
      reader.onload = e => {
        uploadImages.push(e.target.result);
        downLoaded++;
        checkFiles();
      };

      reader.onerror = () => {
        downLoaded++;
        checkFiles();
      };

      reader.readAsDataURL(file);
    });

  };

  deleteImage = img => {
    const { id } = img;
    if (!id) return false;

    if (window.confirm('Вы дейстивтельно хотите удалить ?')) {
      fetcher
        .post('/delete_image', img)
        .then(res => {
          if (res.error) alert(res.error);
          else this.fetchImages();
        })
    }
  };

  uploadImages = () => {
    if (!this.fileInputRef) return;

    const formData = new FormData();
    forEach(this.fileInputRef.files, file => {
      formData.append('files', file, file.name);
    });

    this.setState({ uploadLoading: true });

    fetcher
      .post('/upload_img', formData, { headers: { 'content-type': 'multipart/form-data' } })
      .then(res => {
        if (this.fileInputRef) this.fileInputRef.value = "";
        if (res.success) this.fetchImages();
      });
  };

  render() {
    const { images, uploadImages, uploadLoading } = this.state;

    return (
      <div className="admin-photos-saver">
        <div className="upload-block">
          <p className="title">Загрузка новых фото</p>
          { uploadLoading && <Loader /> }
          <input
            multiple
            accept="image/*"
            type="file"
            ref={r => this.fileInputRef = r}
            onChange={this.onFileInputChange} />
          <div className="prepare-files-list">
            {
              uploadImages.map(src => (
                <div className="admin-preview-image-block" key={src.slice(0, 20)}>
                  <img alt="upload" src={src} />
                </div>
              ))
            }
          </div>
          {
            uploadImages.length > 0 && (
              <Button color="success" onClick={this.uploadImages}>Сохранить</Button>
            )
          }
        </div>
        <div className="upload-block">
          <p className="title">Загруженные фото</p>
          <div className="prepare-files-list">
            {
              images.map(image => (
                <div className="image-item" key={image.id}>
                  <div className="admin-preview-image-block">
                    <img alt={`i_${image.id}`} src={image.src} />
                  </div>
                  <Button color="danger" onClick={() => this.deleteImage(image)}>Удалить</Button>
                </div>
              ))
            }

          </div>
        </div>
      </div>
    );
  }
}

export default AdminPhotos;
