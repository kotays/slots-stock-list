import React from 'react';
import './404.scss';

const Page404 = () => (
  <div className="error-page-wrapper">
    <h1>404 :(</h1>
  </div>
);

export default Page404;
