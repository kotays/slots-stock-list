import React, { PureComponent } from 'react';
import { InputGroup, InputGroupAddon, Input, Button } from 'reactstrap';
import fetcher from '../../helpers/fetcher';

import './login-page.scss';
import withProfile from '../../hocs/withProfile';

class LoginPage extends PureComponent {
  state = {
    login: '',
    pass: '',
  };

  loginHandler = () => {
    const { login, pass } = this.state;
    if (!login || !pass) return false;

    fetcher
      .post('/login', { login, pass })
      .then(res => {
        if(res.success) window.location.href = '/';
        else if (res.error) alert(res.error);
      });
  };

  setValue = (val, name) => {
    this.setState({ [name]: val });
  };

  componentDidMount() {
    const { profile } = this.props;
    if (profile && profile.name) window.location.href = '/';
  }

  render() {
    const { login, pass } = this.state;

    return (
      <div className="main-page-wrapper">
        <div className="login-form">
          <h1>Login</h1>
          <InputGroup className="login-input-group">
            <InputGroupAddon addonType="prepend" className="input-label">Login</InputGroupAddon>
            <Input onChange={e => { this.setValue(e.target.value, 'login') }} />
          </InputGroup>
          <InputGroup className="login-input-group">
            <InputGroupAddon addonType="prepend" className="input-label">Pass</InputGroupAddon>
            <Input type="password" onChange={e => { this.setValue(e.target.value, 'pass') }} />
          </InputGroup>
          <Button
            color="primary"
            className="login-btn"
            disabled={(!login || !pass)}
            onClick={this.loginHandler}>
            Login
          </Button>
        </div>
      </div>
    );
  }
}

export default withProfile(LoginPage);
