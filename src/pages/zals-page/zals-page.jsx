import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import Loader from '../../components/loader';
import fetcher from '../../helpers/fetcher';
import withProfile from '../../hocs/withProfile';

import './zals-page.scss';

const ZalsPage = ({ profile }) => {
  const [{ zals, loading }, setState] = useState({ zals: null, loading: true });

  useEffect(() => {
    if (!loading) return;

    fetcher
      .post('/get_user_zals', { user_id: profile && profile.id })
      .then(res => {
        if (res.error) return alert(res.error);

        setState({
          zals: (res.zals || []),
          loading: false,
        });
    });
  });

  if (loading) return <Loader />;

  return (
    <div className="zals-page">
      <h1>Выберите зал</h1>
      <div className="zals-list">
        {
          zals.map(zal => (
            <Link
              key={zal.id}
              to={`/zal/${zal.id}`}
              className="zal-item"
              style={{ backgroundImage: `url(${zal.imageSrc})` }}>
              <p>{zal.name}</p>
            </Link>
          ))
        }
      </div>
    </div>
  );
};

export default withProfile(ZalsPage);


