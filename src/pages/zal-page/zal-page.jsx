import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import shortid from 'shortid';

import Loader from '../../components/loader';
import ZalUser from '../../components/zal-user';
import { JOIN_TO_ZAL, UPDATE_ZAL_USERS } from '../../constants/socket-actions';
import fetcher from '../../helpers/fetcher';
import withProfile from '../../hocs/withProfile';

import { socketClient } from '../../helpers/socketClient';

class ZalPage extends Component {
  state = {
    zal: null,
    zalUsers: [],
    loading: true,
    addNew: false,
  };

  componentDidMount() {
    this.fetchZal();
    this.setSocketListeners();
  }

  componentWillMount() {
    this.unSetSocketListeners();
  }

  fetchZal = () => {
    const { match: { params: { zal_id } } } = this.props;

    fetcher
      .post('/get_zal', { zal_id })
      .then(res => {
        if (res.error || !res.zal) return alert(res.error);
        const zalUsers = res.zal.zalUsers || [];

        this.setState({
          zal: res.zal,
          zalUsers: zalUsers.sort((a, b) => b.score - a.score),
          loading: false,
          addNew: false,
        }, this.connectToZalRoom);
      })
  };

  connectToZalRoom = () => {
    const { zal: { id } } = this.state;
    if (id) socketClient.emit(JOIN_TO_ZAL, id);
  };

  updateZalUsers = (zalUsers, sendUpdate = false) => {
    const { zal } = this.state;

    this.setState({
      zal: { ...zal, zalUsers },
      addNew: false,
    });

    if (sendUpdate) {
      socketClient.emit(
        UPDATE_ZAL_USERS,
        { zalId: zal.id, zalUsers, save: true }
      );
    }
  };

  onSaveZalUser = zalUser => {
    const { zal: { zalUsers: users } } = this.state;
    const zalUsers = users ? [...users] : [];

    if (zalUser.id === 'new') {
      zalUser.id = shortid.generate();
      zalUsers.push(zalUser);
      this.updateZalUsers(zalUsers, true);
      return;
    }

    const userIndex = zalUsers.findIndex(user => user.id === zalUser.id);
    if (userIndex === -1) return;

    zalUsers[userIndex] = zalUser;
    this.updateZalUsers(zalUsers, true);
  };

  onDeleteZalUser = zalUser => {
    const { zal: { zalUsers: users } } = this.state;
    const zalUsers = users ? [...users] : [];

    if (zalUser.id === 'new') return this.setState({ addNew: false });

    const userIndex = zalUsers.findIndex(user => user.id === zalUser.id);
    if (userIndex === -1) return;

    zalUsers.splice(userIndex, 1);
    this.updateZalUsers(zalUsers, true);
  };


  setSocketListeners = () => {
    this.onUpdate = socketClient.on(UPDATE_ZAL_USERS, zalUsers => this.updateZalUsers(zalUsers));
  };

  unSetSocketListeners = () => {
    if (this.onUpdate) this.onUpdate.off();
  };

  renderBaseBg = () => {
    const { zal: { imageSrc } } = this.state;
    if (!imageSrc) return null;

    return <div className="zal-page-main-bg" style={{ backgroundImage: `url(${imageSrc})` }} />
  };

  renderGuestZal = () => {
    const { zal: { zalUsers: users } } = this.state;
    const zalUsers = users || [];

    return (
      <div className="zal-wrapper">
        {
          zalUsers
            .sort((a, b) => b.score - a.score)
            .map((zalUser, indx) => (
            <ZalUser key={JSON.stringify(zalUser)} zalUser={zalUser} num={indx+1} />
          ))
        }
      </div>
    );
  };

  closeZalAndSave = () => {
    const { zal: { id } } = this.state;
    if (window.confirm('Вы действительно хотите закрыть акцию в данном зале и сохраить статистику')) {
      fetcher
        .post('/close_zal_and_save', { zalId: id })
        .then(res => {
          if (res.error) return alert(res.error);
          window.location.href = '/';
        });
    }
  };

  renderEditorZal = () => {
    const { zal: { zalUsers }, addNew } = this.state;
    const users = zalUsers || [];

    return (
      <div className="zal-wrapper editor">
        <div className="zal-actions">
          { !addNew && <Button color="success" onClick={() => this.setState({ addNew: true })}>Добавить участника</Button> }
          { !addNew && <Button style={{ marginLeft: '1vw' }} color="danger" onClick={this.closeZalAndSave}>Закрыть и сохранить</Button> }
          { addNew && <ZalUser edit zalUser={{ id: 'new' }} onSave={this.onSaveZalUser} onDelete={this.onDeleteZalUser} /> }
        </div>
        {
          users.map((zalUser, indx) => (
            <ZalUser
              edit
              key={JSON.stringify(zalUser)}
              num={indx + 1}
              zalUser={zalUser}
              onSave={this.onSaveZalUser}
              onDelete={this.onDeleteZalUser} />
          ))
        }
      </div>
    )
  };

  render() {
    const { profile } = this.props;
    const { loading } = this.state;

    if (loading) return <Loader />;

    return (
      <div className="zal-page">
        {this.renderBaseBg()}
        <Link to="/" className="home-link" />
        { profile && profile.id && this.renderEditorZal() }
        { (!profile || !profile.id) && this.renderGuestZal() }
      </div>
    );
  }
}

export default withProfile(withRouter(ZalPage));
