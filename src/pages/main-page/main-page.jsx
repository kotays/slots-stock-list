import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import fetcher from '../../helpers/fetcher';
import withProfile from '../../hocs/withProfile';

import './main-page.scss';

const MainPage = ({ profile }) => {
  const { name, isAdmin } = profile || {};

  const logout = () => {
    fetcher
      .post('/logout')
      .then(() => { window.location.reload(); });
  };

  return (
    <div className="main-page-wrapper">
      <div className="nav-list">
        <Button color="primary"><Link to="/zals">Залы</Link></Button>
        { !name && <Button color="success"><Link to="/login">Войти</Link></Button> }
        { name && <Button color="danger" onClick={logout}>Выйти</Button> }
        { name && isAdmin && <Button color="warning"><Link to="/admin">Admin</Link></Button> }
      </div>
    </div>
  );
};

export default withProfile(MainPage);

