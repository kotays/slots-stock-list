import React, { useState } from 'react';
import { Button, Input } from 'reactstrap';
import { isEqual } from 'lodash';

import './admin-user.scss';

const AdminUser = props => {
  const { user, currentUserId, onSave, onDelete } = props;
  const [stateUser, setUser] = useState(user);

  const showSave = !isEqual(user, stateUser);
  const showDelete = !isEqual(user.id, currentUserId);

  const updateUser = (value, name) => {
    setUser({ ...stateUser, [name]: value });
  };

  return (
    <div className="admin-user">
      <div className="admin-user-item">
        <label>Name</label>
        <Input value={stateUser.name} onChange={e => { updateUser(e.target.value, 'name') }} />
      </div>

      <div className="admin-user-item">
        <label>Login</label>
        <Input value={stateUser.login} onChange={e => { updateUser(e.target.value, 'login') }} />
      </div>

      <div className="admin-user-item">
        <label>Pass</label>
        <Input value={stateUser.pass} onChange={e => { updateUser(e.target.value, 'pass') }} />
      </div>

      {
        showSave && (
          <div className="admin-user-item">
            <Button color="success" onClick={() => onSave(stateUser)}>Save</Button>
          </div>
        )
      }

      {
        showDelete && (
          <div className="admin-user-item">
            <Button color="danger" onClick={() => onDelete(stateUser)}>Delete</Button>
          </div>
        )
      }

    </div>
  )
};

export default AdminUser;
