import React from 'react';
import './loader.scss';

const Loader = () => (
  <div className="loader_block">
    <img src="/img/loader.svg" alt="loader" />
  </div>
);

export default Loader;

