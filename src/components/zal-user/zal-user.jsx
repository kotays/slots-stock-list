import React, { useState } from 'react';
import { Input, Button } from 'reactstrap';
import { isEqual } from 'lodash';

import './zal-user.scss';

const ZalUser = props => {
  const { zalUser, edit, num, onSave, onDelete } = props;
  const [stateZalUser, setState] = useState(zalUser);

  const allowSave = !isEqual(zalUser.name, stateZalUser.name);

  const onUpdate = (val, name) => {
    setState({ ...stateZalUser, [name]: val });
  };

  const updateScore = (type) => {
    let score = stateZalUser.score || 0;

    if (type === 'plus') score += 1;

    if (type === 'minus') {
      if (score === 0) return;
      score -= 1;
    }

    onUpdate(score, 'score');

    if (stateZalUser.id !== 'new') {
      onSave({ ...stateZalUser, score });
    }
  };

  if (edit) {
    return (
      <div className="zal-user editable">
        <div className="edit-item">
          <Input placeholder="Имя" value={stateZalUser.name} onChange={e => onUpdate(e.target.value, 'name')} />
        </div>
        <div className="edit-item score-edit">
          <Button color="danger" onClick={() => updateScore('minus')}>-</Button>
          <span className="score">{stateZalUser.score || 0}</span>
          <Button color="success" onClick={() => updateScore('plus')}>+</Button>
        </div>
        <div className="edit-item">
          {
            allowSave && (
              <Button color="success" onClick={() => onSave(stateZalUser)}>Сохранить</Button>
            )
          }
          <Button color="danger" onClick={() => onDelete(stateZalUser)}>Удалить</Button>
        </div>
      </div>
    );
  }


  return (
    <div className="zal-user guest">
      <label className="zal-user-name">
        <span>{`#${num}`}</span>
        <b>{zalUser.name}</b>
      </label>
      <label className="zal-user-score">
        {zalUser.score || 0}
      </label>
    </div>
  );
};

export default ZalUser;

