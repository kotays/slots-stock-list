import React, { useState, useEffect } from 'react';
import { ProfileContext } from '../../contexts';
import Loader from '../loader';
import fetcher from '../../helpers/fetcher';

import './main-wrapper.scss';

const MainWrapper = ({ children }) => {
  const [{ profile, loading }, setState] = useState({ profile: null, loading: true });

  useEffect(() => {
    if (!loading) return;

    fetcher
      .post('/profile')
      .then(({ profile }) => {
        setState({ profile, loading: false });
      });
  });

  if (loading) return <Loader />;

  return (
    <ProfileContext.Provider value={profile}>
      <div className="main_wrapper">
        { React.cloneElement(children, { profile }) }
      </div>
    </ProfileContext.Provider>
  );
};

export default MainWrapper;
