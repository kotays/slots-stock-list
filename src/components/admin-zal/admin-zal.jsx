import React, { useState } from 'react';
import { Button, Input } from 'reactstrap';
import { isEqual } from 'lodash';
import classNames from 'classnames';
import './admin-zal.scss';

const AdminZal = props => {
  const { zal, users, images, onSave, onDelete } = props;
  const [stateZal, setZal] = useState(zal);

  const updateZal = (value, name) => {
    setZal({ ...stateZal, [name]: value });
  };

  const allowSave = !isEqual(zal, stateZal);
  const allowDelete = !isEqual(zal.id, 'new');

  const selectZalUser = (id, checked) => {
    const zalUsers = stateZal.allowUsers ? [...stateZal.allowUsers]: [];
    const userIndex = zalUsers.findIndex(user => user === id);

    if (checked && userIndex === -1) {
      zalUsers.push(id);
    } else if (!checked && userIndex !== -1) {
      zalUsers.splice(userIndex, 1);
    }

    updateZal(zalUsers, 'allowUsers');
  };

  return (
    <div className="admin-zal-item">
      <div className="admin-zal-item-block inline">
        <label>Название</label>
        <Input value={stateZal.name} onChange={e => { updateZal(e.target.value, 'name') }} />
      </div>
      <div className="admin-zal-item-block">
        <label>Пользователи зала</label>
        <div className="flex-list">
          {
            users.map(user => {
              const checked = (stateZal.allowUsers || []).includes(user.id);
              return (
                <label className="zal-user-item" key={user.id}>
                  <input
                    type="checkbox"
                    checked={checked}
                    onChange={() => selectZalUser(user.id, !checked)} />
                  <span>{user.name}</span>
                </label>
              );
            })
          }
        </div>
      </div>
      <div className="admin-zal-item-block">
        <label>Изображение зала</label>
        <div className="admin-zal-images-list flex-list">
          {
            images.map(image => {
              const isSelected = stateZal.imageId === image.id;
              return (
                <div
                  className={classNames("admin-preview-image-block", { active: isSelected })}
                  key={image.id}
                  onClick={() => updateZal(image.id, 'imageId')}>
                  <img alt={`i_${image.id}`} src={image.src} />
                </div>
              );
            })
          }
        </div>
      </div>

      {
        (allowSave || allowDelete) && (
          <div className="admin-zal-item-block">
            { allowDelete && <Button color="danger" onClick={() => onDelete(stateZal)}>Удалить</Button> }
            { allowSave &&  <Button className="save-action" color="success" onClick={() => onSave(stateZal)}>Сохранить</Button> }
          </div>
        )
      }
    </div>
  );
};

export default AdminZal;
