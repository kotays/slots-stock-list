import io from 'socket.io-client';
import { SERVER_BASE_URL } from './envConfig';

export const socketClient = io(SERVER_BASE_URL, { transport: ['websocket'] });

export default socketClient;
