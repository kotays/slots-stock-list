import axios from 'axios';
import { SERVER_BASE_URL } from './envConfig';

const Axios = axios.create({
  baseURL: `${SERVER_BASE_URL}/api/`,
  timeout: 10000,
  withCredentials: true,
  headers: {
    'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
    'Content-Type': 'application/json; charset=utf-8'
  }
});

Axios.interceptors.response.use(
  res => { return res.data; },
  error => { return Promise.reject(error); }
);

export default Axios;


