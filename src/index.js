import React from 'react';
import ReactDOM from 'react-dom';
import { Router as AppRouter, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import 'bootstrap/dist/css/bootstrap.min.css';

import MainWrapper from './components/main-wrapper';
import AdminPage from './pages/admin-page';
import LoginPage from './pages/login-page';
import MainPage from './pages/main-page';
import ZalsPage from './pages/zals-page';
import ZalPage from './pages/zal-page';
import Page404 from './pages/404';


ReactDOM.render(
    <AppRouter history={createBrowserHistory()}>
      <MainWrapper>
        <Switch>
          <Route exact path="/"  component={MainPage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/admin" component={AdminPage} />
          <Route path="/zals" component={ZalsPage} />
          <Route path="/zal/:zal_id" component={ZalPage} />
          <Route path="*" component={Page404} />
        </Switch>
      </MainWrapper>
    </AppRouter>,
  document.getElementById('root')
);
