import React from 'react';
import { ProfileContext } from '../contexts';

const withProfile = ChildComponent => props => (
  <ProfileContext.Consumer>
    { profile => <ChildComponent {...props} profile={profile} /> }
  </ProfileContext.Consumer>
);

export default withProfile;
