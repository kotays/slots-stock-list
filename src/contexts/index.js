import { createContext } from 'react';

const ProfileContext = createContext(null);
const SocketContext = createContext({});

export {
  ProfileContext,
  SocketContext,
}
